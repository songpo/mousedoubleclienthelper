﻿// MouseHelperConsole.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <Windows.h>
#include <iostream>
#include <tchar.h>
#include <conio.h>
#include <strsafe.h>
#include <chrono>

#include <sys\timeb.h>


#define KEY_DOWN(VK_NONAME) ((GetAsyncKeyState(VK_NONAME) & 0x8000) ? 1:0) 

using namespace std;
using namespace std::chrono;

HHOOK mouseHook;

// 最后一次点击时间，从左键弹起开始计时
long long last_click_time = 0;

// 设置检测双击时间为20毫秒
long interval = 20;

// 是否启用
bool enable = true;

// 是否已安装钩子
bool hooked = false;

// 是否卸载钩子
bool unhook = false;

long long getSystemTime() {
	timeb t{};
	ftime(&t);
	return t.time * 1000 + t.millitm;
}

LRESULT __stdcall MouseHookCallback(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode >= 0)
	{
		switch (wParam)
		{
		case WM_LBUTTONDOWN:
			{
				// const auto current_click_time = GetTickCount();
				const auto current_click_time = getSystemTime();
				const auto cost = current_click_time - last_click_time;
				cout << "上次点击时间：" << last_click_time << "，本次点击时间：" << current_click_time << "，间隔：" << cost << " 毫秒" << endl;
				if (last_click_time > 0 && cost < interval)
				{
					cout << "疑似连击，果断取消" << endl;		
					return LRESULT(true);
				}
			}			
			break;

		case WM_LBUTTONUP:
			{
				// last_click_time = GetTickCount();
				last_click_time = getSystemTime();
				// cout << "Left Button Up" << "上次点击时间：" << last_click_time << endl;
			}			
			break;
		}
	}
	return CallNextHookEx(mouseHook, nCode, wParam, lParam);
}

void SetHook()
{
	mouseHook = SetWindowsHookEx(WH_MOUSE_LL, MouseHookCallback, nullptr, 0);
	if (!mouseHook)
	{
		cout << "Failed to install mouse hook!" << endl;
	}
}

void ReleaseHook()
{
	UnhookWindowsHookEx(mouseHook);
}

int main(int argc, char *argv[])
{
	std::cout << "程序运行后，后卡一小会儿，请稍等片刻" << std::endl;

	const auto temp_interval = argv[1];
	if (nullptr != temp_interval)
	{
		interval = atol(argv[1]);
	}

	std::cout << "当前连击检测间隔：" << interval << " 毫秒" << std::endl;

	SetHook();
	hooked = true;

	MSG msg;

	while (true)
	{
		if (KEY_DOWN(VK_F12))
		{
			std::cout << "按下F12" << std::endl;
			enable = !enable;
		}

		if (enable)
		{
			std::cout << "开启连击检测" << std::endl;
			if (!hooked)
			{
				SetHook();
				hooked = true;
				unhook = false;
			}

			if (GetMessage(&msg, nullptr, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		} else
		{
			std::cout << "关闭连击检测" << std::endl;

			if (!unhook)
			{
				ReleaseHook();
				unhook = true;
				hooked = false;
			}			
		}
	}	
	
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
